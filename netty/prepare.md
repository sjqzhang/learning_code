### 对于构造大型系统如何统日志？
```java
private static final InternalLogger logger = InternalLoggerFactory.getInstance(ServerBootstrap.class);
    //InternalLoggerFactory
    public static InternalLogger getInstance(Class<?> clazz) {
        return getInstance(clazz.getName());
    }
	//CommonsLoggerFactory
	public InternalLogger newInstance(String name) {
        return new CommonsLogger(LogFactory.getLog(name), name);
    }
```
