
package main

import (
	"fmt"
	"time"
)

func IF_Test()  {
	var a int
	var b int
	fmt.Println("input a and b")
	fmt.Scanf("%d %d",&a,&b)
	if a==b {
		fmt.Println(fmt.Sprintf("a=%d,b=%d a=b",a,b))
	} else {
		fmt.Println(fmt.Sprintf("a=%d,b=%d a!=b",a,b))
	}



}

func FOR_TEST()  {

	i:=10
	fmt.Println("input i for counter")
	fmt.Scanf("%d",&i)
	for j:=0;j<i;j++ {
		fmt.Println(j)
	}

}

func SWITCH_TEST()  {
	var i interface{}
	fmt.Println("please input i:")
	fmt.Scanf("%v",&i)
	switch i.(type) {
	case int:
		fmt.Println("i is int")
	case string:
		fmt.Println("i is string")
	default:
		fmt.Println("i is default")
	}
}

func SELECT_TEST()  {

	tick:=time.NewTicker(time.Second*5)

	go func() {
		for {
			fmt.Println("a")
			time.Sleep(time.Second*1)
		}
	}()

	select {

	case <-tick.C:
		break


	}

}


func main()  {


	//IF_Test()

	//FOR_TEST()

  //SWITCH_TEST()

	SELECT_TEST()
}