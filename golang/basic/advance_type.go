package main

import (
	"fmt"
	"time"
)

type Student struct {
	Name string
	Age int
	Addr string

}

func CUSTOM_TYPE()  {
	stu:=Student{Name:"zhang shan",Age:22,Addr:"SZ"}

	fmt.Println(stu)

}

func ARRAY_TEST()  {

	var students[3] Student

	students[0]=Student{Name:"zhang shan",Age:22,Addr:"SZ"}
	students[1]=Student{Name:"li shi",Age:23,Addr:"HZ"}
	students[2]=Student{Name:"wang wu",Age:24,Addr:"BJ"}

	for i:=0;i<len(students);i++ {
		fmt.Println(students[i].Name)
	}

	part:=students[1:]

	fmt.Println(part)

	part=append(part,Student{"ma lu",26,"SH"})

	fmt.Println(part)



}

func MAP_TEST()  {
	var key string
	fmt.Println("please input a key")
	fmt.Scanf("%s",&key)
	m:=make(map[string]interface{})
	m["name"]="zhang shan"
	m["age"]=25
	m["addr"]="Shen Zhen"
	fmt.Println(m)

	if name,ok:=m[key];ok {
		fmt.Println(name)
	} else {
		println(fmt.Sprintf("not found %v in map",key))
	}

}

func CHAN_TEST()  {
	c:=make(chan interface{},10)
	finish:=make(chan bool,1)
	producer:= func() {
		for i:=0;i<30;i++  {
			c<-i
			fmt.Println("producer ",i)
		}
	}
	comsumer:= func() {
		for   {
			j:=<-c
			switch j.(type) {
			case int:
				if j.(int)==29 {
					finish<-true
				}
			}
			fmt.Println("consumer ",j)
			time.Sleep(time.Millisecond*100)
		}
	}
	go producer()
	go comsumer()
	select {
		case <-finish:
			break
	}
}

func main()  {


	//MAP_TEST()


//CHAN_TEST()

ARRAY_TEST()

}